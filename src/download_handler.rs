/*
*   Copyright (c) 2020 Johannes Thorén
*   All rights reserved.

*   Permission is hereby granted, free of charge, to any person obtaining a copy
*   of this software and associated documentation files (the "Software"), to deal
*   in the Software without restriction, including without limitation the rights
*   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*   copies of the Software, and to permit persons to whom the Software is
*   furnished to do so, subject to the following conditions:

*   The above copyright notice and this permission notice shall be included in all
*   copies or substantial portions of the Software.

*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
*   SOFTWARE.
*/

use reqwest;
use std::fs::File;
use std::io;
use std::io::Write;
/// checks if the string provided contains a char that windows can't use to name a file or directory.
/// used if you whant to name the file downloaded the same as the post.
pub fn special_char_handler(str_to_check: &mut String) -> String {
    let special_chars = vec!["\\", "/", "\"", "?", ":", "*", "<", ">", "|"];

    for char in special_chars {
        while str_to_check.contains(char) {
            str_to_check.remove(str_to_check.find(char).unwrap());
        }
    }
    str_to_check.parse().unwrap()
}

/// checks if the post_data_url is a valid "reddit" url, else returns false.
pub fn check_url(post_data_url: &str) -> bool {
    let mut valid = false;

    let valid_url = vec![
        //"https://i.imgur.com",
        //"https://imgur.com",
        "https://i.redd.it",
        "https://www.redditstatic.com",
        "https://b.thumbs.redditmedia.com",
    ];

    for url in valid_url {
        if post_data_url.starts_with(url) {
            valid = true;
        }
    }

    valid
}

/// gets the file type of the picture from a post.
fn get_filetype(post_data_url: &str) -> String {
    let split = post_data_url.split(".");
    let vec = split.collect::<Vec<&str>>();
    vec[vec.len() - 1].to_string()
}

/// download the picture from a post.
pub fn download(post_data_url: &str, post_title: &mut String, file_dest: &str) {

    if check_url(post_data_url) {
        // this will make it possible to download stuff from imgur

        //TODO make this work
        /*let mut post_data_url = post_data_url.to_string();
        if post_data_url.starts_with("https://i.imgur.com") {
            let parts = post_data_url.split_at(20);
            let parts = (parts.0.to_string(), parts.1.to_string());
            post_data_url.clear();
            post_data_url.push_str(&parts.0);
            post_data_url.push_str("download/");
            post_data_url.push_str(&parts.1);
        }
        else if post_data_url.starts_with("https://imgur.com") {
            let parts = post_data_url.split_at(18);
            let parts = (parts.0.to_string(), parts.1.to_string());
            post_data_url.clear();
            post_data_url.push_str(&parts.0);
            post_data_url.push_str("download/");
            post_data_url.push_str(&parts.1);
        }*/

        let out = File::create(format!(
            "{}{}.{}",
            file_dest,
            special_char_handler(post_title),
            get_filetype(&post_data_url)
        ));

        println!("{}{}.{}",
        file_dest,
        special_char_handler(post_title),
        get_filetype(&post_data_url));

        match out {
            Ok(mut x) => match io::copy(&mut reqwest::get(post_data_url).unwrap(), &mut x) {
                Ok(_) => {}
                Err(e) => println!("\n\n\n{}", e),
            },
            Err(e) => println!("\n\n\n{}", e),
        };
    } else {
        let mut out = File::create(format!("{}{}.html",file_dest ,special_char_handler(post_title)))
            .expect("could not create file!");

        let data = format!(
            "<meta http-equiv=\"Refresh\" content=\"0; url={}\"/>",
            post_data_url
        );

        out.write_all(data.as_bytes())
            .expect("could not write data to file!");
    }
}
